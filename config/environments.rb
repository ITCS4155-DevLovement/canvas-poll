require 'yaml'
#The environment variable DATABASE_URL should be in the following format:
# => postgres://{user}:{password}@{host}:{port}/path
configure :production, :development do
	
	# db = URI.parse(ENV['DATABASE_URL'] || 'postgres://localhost/polls_development')
	config = YAML.load_file("config/database.yml")["development"]
	puts config
	ActiveRecord::Base.establish_connection(
			:adapter => config["adapter"] == 'postgres' ? 'postgresql' : config["adapter"],
			:host     => config["host"],
			:username => config["user"],
			:password => config["password"],
			:database => config["database"],
			:encoding => 'utf8'
	)
end
