begin
  require 'rubygems'
rescue LoadError
  puts "You must install rubygems to run this example"
  raise
end

begin
  require 'bundler/setup'
rescue LoadError
  puts "to set up this example, run these commands:"
  puts "  gem install bundler"
  puts "  bundle install"
  raise
end

require 'sinatra'
require 'oauth'
require 'oauth/request_proxy/rack_request'
require 'active_record'

require './config/environments'
require './models/poll'
require './models/question'
require './models/answer'
require './models/user_answer'

# hard-coded oauth information for testing convenience
$oauth_key = "test"
$oauth_secret = "secret"

# sinatra wants to set x-frame-options by default, disable it
disable :protection
# enable sessions so we can remember the launch info between http requests, as
# the user takes the assessment
enable :sessions

# this is the entry action that Canvas (the LTI Tool Consumer) sends the
# browser to when launching the tool.
post "/assessment/start" do
  # first we have to verify the oauth signature, to make sure this isn't an
  # attempt to hack the planet
  begin
    signature = OAuth::Signature.build(request, :consumer_secret => $oauth_secret)
    signature.verify() or raise OAuth::Unauthorized
  rescue OAuth::Signature::UnknownSignatureMethod,
         OAuth::Unauthorized
    return %{unauthorized attempt. make sure you used the consumer secret "#{$oauth_secret}"}
  end
  
  # make sure this is an assignment tool launch, not another type of launch.
  # only assignment tools support the outcome service, since only they appear
  # in the Canvas gradebook.

  # unless params['lis_outcome_service_url'] && params['lis_result_sourcedid']
  # #unless params['resource_link_id'] && params['lis_outcome_service_url']
  #   return %{It looks like this LTI tool wasn't launched as an assignment, or you are trying to take it as a teacher rather than as a a student. Make sure to set up an external tool assignment as outlined <a target="_blank" href="https://github.com/instructure/lti_example">in the README</a> for this example.}
  # end

  # store the relevant parameters from the launch into the user's session, for
  # access during subsequent http requests.
  # note that the name and email might be blank, if the tool wasn't configured
  # in Canvas to provide that private information.
  # 'roles' stores information about the current user's role. 'Learner' for student and 'Instructor' for teacher account
  %w(roles lis_outcome_service_url lis_result_sourcedid lis_person_name_full lis_person_contact_email_primary).each { |v| session[v] = params[v] }

  # that's it, setup is done. now send them to the assessment!
  redirect to("/assessment")
end

def username
  session['lis_person_name_full'] || 'friend'
end

get "/create" do
  erb :create  
end


post "/create" do
  # take input from create
  puts params
  @poll = Poll.create(name: params["quiz_title"],
                  course_id: 1,
                  is_open: true)
  
  #@poll.questions.new
  questions = params.keys.select{ |i| i =~ /^q\d$/ }
  all_answers = params.keys.select{ |i| i =~ /^q\da\d$/ }
  
  answer_keys = params.keys.select{ |i| i =~ /^q\dr$/ }
  correct_answers = answer_keys.map{ |i| params[i] }  
  
  questions.each { |q|
    # question = Question.new
    question = @poll.questions.create(text: params[q])
    
    answers = all_answers.select{ |i| i =~ /^#{q}/ }
      answers.each{ |a|
        question.answers.create(
            text: params[a],
            weight: is_answer_correct?(correct_answers, a) ? 1 : 0,
            votes: 0)
      }
    }
  
  redirect to("/home")
end

get "/poll/:id" do
  @poll = Poll.find(params[:id])
  puts "", @poll.inspect
  put_poll(:id)
  
  redirect to("/home")
end

get "/edit" do
  erb :edit
end

get "/host" do
  @polls = Poll.where(is_open: true, course_id: 1)
  # puts "...", @polls.inspect, "..."
  erb :host
end

get '/home' do 
  erb :instructor
end


get '/test' do
  # show teacher hosted material
  erb :test
end


#for testing the view of learner.erb
get '/learner' do
  erb :learner
end
  
get "/assessment" do
  # first make sure they got here through a tool launch
  # unless session['lis_result_sourcedid']
  #   return %{You need to take this assessment through Canvas.}
  # end
  
  if session['roles'] == 'Instructor' 
    # render teacher view
    erb :instructor
    
    
  elsif session['roles'] == 'Learner'
    # render student view
    #erb :learner
    erb :test
    #redirect to("/assessment")
    
  else
    # this shouldn't happen. throw some sort of error
  end

  # now render a simple form the user will submit to "take the quiz"
  #erb :main
end

# This is the action that the form submits to with the score that the student entered.
# In lieu of a real assessment, that score is then just submitted back to Canvas.
post "/assessment" do
  # obviously in a real tool, we're not going to let the user input their own score
  score = params['score']
  if !score || score.empty?
    redirect to("/assessment")
  end

  # now post the score to canvas. Make sure to sign the POST correctly with
  # OAuth 1.0, including the digest of the XML body. Also make sure to set the
  # content-type to application/xml.
  xml = %{
<?xml version = "1.0" encoding = "UTF-8"?>
<imsx_POXEnvelopeRequest xmlns = "http://www.imsglobal.org/lis/oms1p0/pox">
  <imsx_POXHeader>
    <imsx_POXRequestHeaderInfo>
      <imsx_version>V1.0</imsx_version>
      <imsx_messageIdentifier>12341234</imsx_messageIdentifier>
    </imsx_POXRequestHeaderInfo>
  </imsx_POXHeader>
  <imsx_POXBody>
    <replaceResultRequest>
      <resultRecord>
        <sourcedGUID>
          <sourcedId>#{session['lis_result_sourcedid']}</sourcedId>
        </sourcedGUID>
        <result>
          <resultScore>
            <language>en</language>
            <textString>#{score}</textString>
          </resultScore>
        </result>
      </resultRecord>
    </replaceResultRequest>
  </imsx_POXBody>
</imsx_POXEnvelopeRequest>
  }
  consumer = OAuth::Consumer.new($oauth_key, $oauth_secret)
  token = OAuth::AccessToken.new(consumer)
  response = token.post(session['lis_outcome_service_url'], xml, 'Content-Type' => 'application/xml')

  headers 'Content-Type' => 'text'
  %{
Your score has #{response.body.match(/\bsuccess\b/) ? "been posted" : "failed in posting"} to Canvas. The response was:
  #{response.body}
  }
end

def is_answer_correct?(correct_answers, answer)
  correct_answers.any? { |s| 
    s.include? answer.match(/^q\da(\d)$/).captures[0]
  }
end
	
def put_poll(id)
  poll = Poll.find(params[:id])
  puts "", poll.inspect
  poll.questions.each{ |q| 
    puts q.inspect
    q.answers.each{ |a|
      puts a.inspect
    }
  }
  puts ""
end
