# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20161206162941) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "answers", force: :cascade do |t|
    t.string  "text"
    t.decimal "weight"
    t.integer "votes"
    t.integer "question_id"
    t.index ["question_id"], name: "index_answers_on_question_id", using: :btree
  end

  create_table "polls", force: :cascade do |t|
    t.string  "name"
    t.integer "course_id"
    t.boolean "is_open"
  end

  create_table "questions", force: :cascade do |t|
    t.string  "text"
    t.integer "poll_id"
    t.index ["poll_id"], name: "index_questions_on_poll_id", using: :btree
  end

  create_table "user_answer", force: :cascade do |t|
    t.integer "user_id"
    t.integer "answer_id"
    t.index ["answer_id"], name: "index_user_answer_on_answer_id", using: :btree
  end

  add_foreign_key "answers", "questions"
  add_foreign_key "questions", "polls"
  add_foreign_key "user_answer", "answers"
end
