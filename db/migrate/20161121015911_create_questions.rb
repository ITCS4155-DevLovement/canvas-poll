class CreateQuestions < ActiveRecord::Migration[5.0]
  def up
  	create_table :questions do |t|
  		t.string :text
  		t.references :poll, foreign_key: true
  	end
  end

  def down
  	drop_table :questions
  end
end
