class CreatePolls < ActiveRecord::Migration
  def up
  	create_table :polls do |t|
  		t.string :name
  		t.integer :course_id
  		t.boolean :is_open
  	end
  end

  def down
  	drop_table :polls
  end
end