class CreateUserAnswer < ActiveRecord::Migration[5.0]
  def change
    create_table :user_answer do |t|
      t.integer :user_id
      t.references :answer, foreign_key: true
    end
  end
end
